
    - Description of the problem and solution:
    
    This is iOS demo app for working with https://newsapi.org API in SWIFT language
    that shows working with design patterns, SOLID principles, Swift language usage,
    cocoa pods and modern pods usage
    
    - How to build/deploy/use your solution. Link to the hosted application if applicable:
    
    Just clone, update or install pods and run
    
    - Reasoning behind your technical choices, including architectural:
    
    Used SWIFT 3 recommendations such as using struct everywhere when possible
    if there no memory overhead with bulk copy of structs or we dont need
    sharing by reference objects and no need for inheritance. As well as we could
    easily convert struct to class if needed afterwards.
    
    Using singleton for accessing common objects like for web request operations.
    
    Enum used with switch expression instead of if...else expression just not to
    left out any choice.
    
    Using different SWIFT features for concise code like using subscripts and 
    extensions
    
    Separating delegates to different classes just to make controllers more light
    