//
//  WebVC.swift
//  NewsApi
//
//  Created by Rufat A on 2/24/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

class WebVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    var article: NAArticle!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: WebViewDelegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        NAUtils.hideHUD(contentView: webView);
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        NAUtils.hideHUD(contentView: webView);
    }

    // MARK: UserDefined

    func loadData() {
        if let url = URL(string: article.url) {
            NAUtils.showHUD(contentView: webView);
            let request = URLRequest(url: url)
            webView.loadRequest(request)
            webView.backgroundColor = UIColor.clear
            self.view.backgroundColor = UIColor.clear
        }
    }

    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
