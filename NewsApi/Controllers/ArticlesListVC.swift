//
//  ArticlesListVC.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

// controller shows all articles of selected source

class ArticlesListVC: UIViewController {
    
    @IBOutlet weak var tblArticleList: UITableView!
    
    // table view for source listing
    
    var dsArticle: ArticleDS!
    var tvArticle: ArticleTV!

    var source: NASource!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.bindData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }
    
    deinit {
        dsArticle = nil
        tvArticle = nil
        print("deinit ArticlesListVC")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let identifier = segue.identifier {
            switch identifier {
            case NASegues.showArticleDetailSegue.rawValue:
                if let selectedArticleIndex = tblArticleList.indexPathForSelectedRow,
                    let theSelectedArticle = dsArticle.getArticle(selectedArticleIndex),
                    let vc = segue.destination as? WebVC {
                    
                    // pass selected sources to articles page to load it
                    
                    vc.article = theSelectedArticle
                    
                    // deselect row at sources table
                    
                    tblArticleList.deselectRow(at: selectedArticleIndex, animated: true)
                }
            default: break
            }
        }
    }
 
    // MARK: UserDefined
    
    func bindData() {
        
        // bind Article table
        
        dsArticle = ArticleDS()
        tvArticle = ArticleTV()
        tblArticleList.dataSource = dsArticle
        tblArticleList.delegate = tvArticle
        
        self.title = source.name
    }
    
    func loadData() {
        guard let theSource = self.source else { return }
        
        NARequest.sharedInstance.getArticles(sourceId: theSource.id, sortBy: theSource.sortBysAvailable[0].rawValue, controller: self, isSilent: true, onSuccess: { [weak self] json  in
            guard let strongSelf = self else { return }
            
            strongSelf.dsArticle.populateFromJson(json["articles"])
            strongSelf.tblArticleList.reloadData()
            
        } )
    }
}
