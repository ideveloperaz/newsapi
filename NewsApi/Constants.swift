//
//  Constants.swift
//  NewsApi
//
//  Created by Rufat A on 2/22/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation


// MARK: Configuration settings

struct NAConfig {
    
    // APIKEY to get articles from newsapi.org
    
    static let APIKEY = "262fcd3d295b4304ba053ac59ef70492"
    
    // base url for newsapi web site
    
    static let baseUrl = "https://newsapi.org/"
    
    // get links
    
    static func getLink(_ link: NALinks, _ sourceId: String = "", _ sort: String = "latest") -> String {
        switch link {
        case .sources_url:
            return "\(baseUrl)v1/sources?language=en"
        case .articles_url:
            return "\(baseUrl)v1/articles?source=\(sourceId)&sortBy=\(sort)&apiKey=\(APIKEY)"
        }

    }
}

// MARK: Commonly used Enums and Structs

// Category Used to store given categories of Sources

enum NACategory: String {
    case all = "all categories"
    case business = "business"
    case entertainment = "entertainment"
    case gaming = "gaming"
    case general = "general"
    case music = "music"
    case scienceAndNature = "science-and-nature"
    case sport = "sport"
    case technology = "technology"
    
    static let allValues = [all, business, entertainment, gaming, general, music, scienceAndNature, sport, technology]
}

// Available sort features for Sources

enum NASortBysAvailable: String {
    case top = "top"
    case latest = "latest"
    case popular = "popular"
}

// existing segues

enum NASegues: String {
    case showArticlesSegue = "show-articles-segue"
    case showArticleDetailSegue = "show-article-detail-segue"
}

// Enum of links used in app

enum NALinks{
    case sources_url
    case articles_url
}

// logos for Articles structure

struct NAUrlsToLogo {
    let small: String
    let medium: String
    let large: String
}

// MARK: Protocols declaration

protocol NACategoryChangedProtocol: class {
    func categoryDidChange(_ category: String)
}
