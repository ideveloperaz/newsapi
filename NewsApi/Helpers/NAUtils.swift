//
//  NAUtils.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit
import MBProgressHUD

// different helper functions for code reuse purpose

class NAUtils: NSObject {

    // show alert helper
    
    class func showAlert(
        title: String,
        message: String,
        controller: UIViewController
    ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }

    // show alert helper with completion handler

    class func showAlert(
        title: String,
        message: String,
        controller: UIViewController,
        completion: @escaping ()->Void
    ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { (action: UIAlertAction) -> Void in
            completion()
        } )
        controller.present(alert, animated: true, completion: nil)
    }
    
    // show loading indicator
    
    class func showHUD(contentView: UIView) {
        if contentView.viewWithTag(2121) == nil {
            let hud = MBProgressHUD.showAdded(to: contentView, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.label.text = "Loading..."
            hud.tag = 2121
        }

    }
    
    // hide loading indicator
    
    class func hideHUD(contentView: UIView) {
        MBProgressHUD.hide(for: contentView, animated: true)
    }
    
}
