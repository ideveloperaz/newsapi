//
//  NARequest.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

// This class used to handle all request to web
// This is Singleton class

class NARequest: NSObject {
    
    // singleton instance of the class
    
    static let sharedInstance = NARequest()
    
    // shared instance od Alamofire SessionManager
    
    var sessionManager: SessionManager?
    let debug = 0
    
    // init Alamofire session manager
    // private init - so there only one instance (singleton)
    
    private override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    deinit {
        sessionManager = nil
        print("deinit NARequest")
    }
    
    // base request function to handle all requests
    
    func request(
        useHud: Bool,
        link : String,
        parameters: [String: String],
        controller: UIViewController,
        isSilent: Bool = true,
        onSuccess: @escaping (JSON)->Void
    ) {
        if useHud { NAUtils.showHUD(contentView: controller.view); }
        
        self.sessionManager?.request(link, method: .get, parameters: parameters)
            .responseJSON { [weak controller, weak self] response in
                guard let strongVC = controller else { return }
                guard let strongSelf = self else { return }
                
                if useHud { NAUtils.hideHUD(contentView: strongVC.view); }
                
                strongSelf.parseResponce(response: response,
                                         strongController: strongVC,
                                         strongSelf: strongSelf,
                                         isSilent: isSilent,
                                         onSuccess: onSuccess,
                                         onFailure: { _ in })
        }
    }
    
    // parse responce from web request and pass data to completion handler
    // use SwiftyJSON to parse response data safely
    
    func parseResponce(
        response: DataResponse<Any>,
        strongController: UIViewController,
        strongSelf: NARequest,
        isSilent: Bool = false,
        onSuccess: @escaping (JSON)->Void,
        onFailure: @escaping (String)->Void
    ) {
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            if strongSelf.debug == 1 { print(json) }
            
            if let theStatus = json["status"].string,
                theStatus == "ok" {
                onSuccess(json)
            } else {
                if !isSilent { NAUtils.showAlert(title: "FAILURE", message: json["status"].stringValue, controller: strongController) }
                onFailure(json["status"].stringValue)
            }
        case .failure(let error):
            print(error)
            if !isSilent { NAUtils.showAlert(title: "FAILURE", message: error.localizedDescription, controller: strongController) }
            onFailure(error.localizedDescription)
        }
    }
    
    // function to get sources from web
    
    func getSources(
        controller: UIViewController,
        isSilent: Bool = true,
        onSuccess: @escaping (JSON)->Void
    ) {
        self.request(useHud: true, link: NAConfig.getLink(.sources_url), parameters: [:], controller: controller, onSuccess: onSuccess)
    }

    
    // function to get articles of selected source from web
    
    func getArticles(
        sourceId: String,
        sortBy: String,
        controller: UIViewController,
        isSilent: Bool = true,
        onSuccess: @escaping (JSON)->Void
    ) {
        self.request(useHud: true, link: NAConfig.getLink(.articles_url, sourceId, sortBy) , parameters: [:], controller: controller, onSuccess: onSuccess)
    }

}
