//
//  Source.swift
//  NewsApi
//
//  Created by Rufat A on 2/22/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// Source structure that represent one source

struct NASource {
    let id: String
    let name: String
    let description: String
    let url: String
    let category: NACategory
    let language: String
    let country: String
    let urlsToLogo: NAUrlsToLogo
    let sortBysAvailable: [NASortBysAvailable]
}
