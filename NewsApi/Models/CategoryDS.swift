//
//  CategoryDS.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit

// delegate for data source of predefined categories for popup category table

class CategoryDS: NSObject, UITableViewDataSource {
    
    // MARK: UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NACategory.allValues.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.reuseCellIdentifier(), for: indexPath ) as! CategoryCell
        
        cell.textLabel?.text = NACategory.allValues[indexPath.row].rawValue
        
        return cell
    }
    
}
