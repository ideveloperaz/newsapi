//
//  DSSource.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AlamofireImage

// delegate class to represent data source for sources

class SourceDS: NSObject, UITableViewDataSource {
    
    var sourceList: NASourceList?
    
    // used for filtering currently selected category
    
    var selectedCategory: NACategory?
    
    override init() {
        print("deinit SourceDS")
    }
    
    init(sourceList: NASourceList) {
        self.sourceList = sourceList
        self.selectedCategory = nil
    }
    
    // populate sources from web request
    
    func populateFromJson(_ json: JSON) {
        var sourceArray = [NASource]()
        
        for (_,subJson):(String, JSON) in json {
            
            guard let theId = subJson["id"].string else { continue }
            guard let theName = subJson["name"].string else { continue }
            guard let theDescription = subJson["description"].string else { continue }
            guard let theUrl = subJson["url"].string else { continue }
            guard let theCategory = subJson["category"].string else { continue }
            guard let theLanguage = subJson["language"].string else { continue }
            guard let theCountry = subJson["country"].string else { continue }
            guard let theUrlsToLogos = subJson["urlsToLogos"].dictionary else { continue }
            guard let theSortBysAvailable = subJson["sortBysAvailable"].array else { continue }

            guard let theSmall = theUrlsToLogos["small"]?.string else { continue }
            guard let theMedium = theUrlsToLogos["medium"]?.string else { continue }
            guard let theLarge = theUrlsToLogos["large"]?.string else { continue }
            
            // convert sort options to array of enums
            
            let theSort = theSortBysAvailable.map({ element -> NASortBysAvailable in
                return NASortBysAvailable(rawValue: element.stringValue)!
            })
            
            sourceArray.append(NASource(id: theId,
                                 name: theName.uppercased(),
                                 description: theDescription,
                                 url: theUrl,
                                 category: NACategory(rawValue: theCategory)!,
                                 language: theLanguage,
                                 country: theCountry,
                                 urlsToLogo: NAUrlsToLogo(small: theSmall, medium: theMedium, large: theLarge),
                                 sortBysAvailable: theSort))
        }
        
        if sourceArray.count > 0 {
            self.sourceList = NASourceList(sourceList: sourceArray)
        }
    }
    
    // get selected source considering that there could be filtration by category
    
    func getSource(_ atIndex: IndexPath) -> NASource? {
        if let theSourceList = sourceList {
            if let theSelectedCategory = self.selectedCategory,
                theSelectedCategory != NACategory.all {
                return theSourceList[theSelectedCategory]?[atIndex.row] ?? nil
            } else {
                return theSourceList.sourceList[atIndex.row]
            }
        } else {
            return nil
        }
    }
    
    
    // MARK: UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theSourceList = sourceList {
            if let theSelectedCategory = self.selectedCategory,
                theSelectedCategory != NACategory.all {
                return theSourceList[theSelectedCategory]?.count ?? 0
            } else {
                return theSourceList.sourceList.count
            }
        } else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SourceCell.reuseCellIdentifier(), for: indexPath ) as! SourceCell
        
        if let theSourceList = sourceList {
            var theSource: [NASource]!
            
            if let theSelectedCategory = self.selectedCategory,
                theSelectedCategory != NACategory.all {
                theSource = theSourceList[theSelectedCategory]
            } else {
                theSource = theSourceList.sourceList
            }
            
            cell.lblName.text = theSource[indexPath.row].name
            cell.lblDescription.text = theSource[indexPath.row].description
            cell.lblCategory.text = theSource[indexPath.row].category.rawValue
            
            // load logo
            // af_setImage - Setting the image with a URL will asynchronously download the image and set it once the request is finished.
            // If the image is cached locally, the image is set immediately.

            let placeholderImage = UIImage(named: "img_placeholder")!
            let downloadURL = URL(string: theSource[indexPath.row].urlsToLogo.small)!
                        
            cell.ivLogo.af_setImage(withURL: downloadURL, placeholderImage: placeholderImage)
            
        }
        
        return cell
    }
    
}
