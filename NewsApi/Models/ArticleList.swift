//
//  ArticleList.swift
//  NewsApi
//
//  Created by Rufat A on 2/22/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// structure that represents articles of selected source

struct NAArticleList {
    
    // In complex cases we could use classes and have a weak ref. to source object here instead of hardcoded string
    
    let source: String
    let sortBy: NASortBysAvailable
    let articleList: [NAArticle]
    
}
