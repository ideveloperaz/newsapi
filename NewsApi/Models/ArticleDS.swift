//
//  ArticleDS.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AlamofireImage

// data source delegte for acticles table view

class ArticleDS: NSObject, UITableViewDataSource {
    
    var articleList: NAArticleList?
    
    override init() {
        self.articleList = nil
    }
    
    deinit {
        print("deinit ArticleDS")
    }
    
    init(articleList: NAArticleList) {
        self.articleList = articleList
    }
    
    // populate articles from web request
    
    func populateFromJson(_ json: JSON) {
        var articleArray = [NAArticle]()
        
        for (_,subJson):(String, JSON) in json {
            
            guard let theAuthor = subJson["author"].string else { continue }
            guard let theTitle = subJson["title"].string else { continue }
            guard let theDescription = subJson["description"].string else { continue }
            guard let theUrl = subJson["url"].string else { continue }
            guard let theUrlToImage = subJson["urlToImage"].string else { continue }
            guard let thePublishedAt = subJson["publishedAt"].string else { continue }
            
            articleArray.append(NAArticle(author: theAuthor,
                                 title: theTitle,
                                 description: theDescription,
                                 url: theUrl,
                                 urlToImage: theUrlToImage,
                                 publishedAt: thePublishedAt))
        }
        
        if articleArray.count > 0 {
            self.articleList = NAArticleList(source: "xxx",
                                             sortBy: .latest,
                                             articleList: articleArray)
        }
    }
    
    // get selected article
    
    func getArticle(_ atIndex: IndexPath) -> NAArticle? {
        if let theArticleList = articleList {
            return theArticleList.articleList[atIndex.row]
        } else {
            return nil
        }
    }
    
    
    // MARK: UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theArticleList = articleList {
            return theArticleList.articleList.count
        } else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.reuseCellIdentifier(), for: indexPath ) as! ArticleCell
        
        if let theArticleList = articleList {
            
            cell.lblName.text = theArticleList.articleList[indexPath.row].title
            cell.lblDescription.text = theArticleList.articleList[indexPath.row].description
            cell.lblSubTitle.text = theArticleList.articleList[indexPath.row].author
            
            // load logo
            // af_setImage - Setting the image with a URL will asynchronously download the image and set it once the request is finished.
            // If the image is cached locally, the image is set immediately.
            
            let placeholderImage = UIImage(named: "img_placeholder")!
            let downloadURL = URL(string: theArticleList.articleList[indexPath.row].urlToImage)!
            
            cell.ivLogo.af_setImage(withURL: downloadURL, placeholderImage: placeholderImage)
        }
        
        return cell
    }
    
}
