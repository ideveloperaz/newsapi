//
//  SourceList.swift
//  NewsApi
//
//  Created by Rufat A on 2/22/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// Source List that represent list of all sources

struct NASourceList {
    let sourceList: [NASource]
    
    // returns filtered list of Sources by given category
    // read only subscript used here for ease of use
    
    subscript(category: NACategory) -> [NASource]? {
        get {
            return self.sourceList.filter { $0.category == category }
        }
    }
}
