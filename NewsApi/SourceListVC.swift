//
//  ViewController.swift
//  NewsApi
//
//  Created by Rufat A on 2/22/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

// main controller that shows user all sources from web site

class SourceListVC: UIViewController, NACategoryChangedProtocol {

    @IBOutlet weak var tblSourceList: UITableView!
    @IBOutlet weak var btnCategory: UIButton!
    
    // isLoadFromWeb used to load data from web only when it is needed
    
    var isAllowedLoadFromWeb = true
    
    // table delegates for source listing
    
    var dsSource: SourceDS!
    var tvSource: SourceTV!

    // table delegates for category filtering
    
    var tblCategory: UITableView!
    var dsCategory: CategoryDS!
    var tvCategory: CategoryTV!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindData()
    }
    
    deinit {
        dsSource = nil
        tvSource = nil
        dsCategory = nil
        tvCategory = nil
        tblCategory = nil
        print("deinit SourceListVC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (isAllowedLoadFromWeb) { loadData() }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        tblCategory.isHidden = true
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        tblCategory.isHidden = true
        
        if let identifier = segue.identifier {
            switch identifier {
            case NASegues.showArticlesSegue.rawValue:
                if let selectedSourceIndex = tblSourceList.indexPathForSelectedRow,
                    let theSelectedSource = dsSource.getSource(selectedSourceIndex),
                    let vc = segue.destination as? ArticlesListVC {
                    
                    // pass selected sources to articles page to load it
                    
                    vc.source = theSelectedSource
                    
                    // deselect row at sources table
                    
                    tblSourceList.deselectRow(at: selectedSourceIndex, animated: true)
                }
            default: break
            }
        }
    }
    
    // function called on back button pressed in ArticlesListVC to prevent loading data from web
    
    @IBAction func unwindToSourceListVC(segue: UIStoryboardSegue) {
        
        //pass here data back to parent controller
        
        self.isAllowedLoadFromWeb = false
    }
    
    // MARK: NACategoryChangedProtocol
    
    func categoryDidChange(_ category: String) {
        self.dsSource.selectedCategory = NACategory(rawValue: category)
        self.tblSourceList.reloadData()
    }
    
    //MARK: UserDefined
    
    func bindData() {
        
        // bind Source table
        
        dsSource = SourceDS()
        tvSource = SourceTV()
        tblSourceList.dataSource = dsSource
        tblSourceList.delegate = tvSource
        
        // create and bind Category table
        
        tblCategory = UITableView()
        dsCategory = CategoryDS()
        tvCategory = CategoryTV()
        tvCategory.delegate = self;             // delegate for NACategoryChangedProtocol
        tblCategory.dataSource = dsCategory
        tblCategory.delegate = tvCategory
        tblCategory.isHidden = true
        
        // register custom cell class (because we create it programmatically)
        
        tblCategory.register(CategoryCell.self, forCellReuseIdentifier: CategoryCell.reuseCellIdentifier())
        self.view.addSubview(tblCategory)
    }
    
    func loadData() {
        
        NARequest.sharedInstance.getSources(controller: self, isSilent: true, onSuccess: { [weak self] json  in
            guard let strongSelf = self else { return }
            
            strongSelf.dsSource.populateFromJson(json["sources"])
            strongSelf.tblSourceList.reloadData()
        } )
    }
    
    @IBAction func btnCategoryClicked(_ sender: UIButton) {
        
        // position category table view under top panel
        
        alignPopupTable()
        tblCategory.isHidden = !tblCategory.isHidden
    }
    
    func alignPopupTable() {
        var frmCategory = btnCategory.frame
        frmCategory.origin.y = UIApplication.shared.statusBarFrame.height + (self.navigationController?.navigationBar.frame.size.height)!
        frmCategory.size.height = frmCategory.size.height * 10
        frmCategory.origin.x -= 30
        frmCategory.size.width += 60
        tblCategory.frame = frmCategory
    }
    
}

