//
//  ArticleCell.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

//custom cell for article table view

class ArticleCell: UITableViewCell {
    
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    class func reuseCellIdentifier() -> String {
        return "ArticleCellIdentifier"
    }
    
}
