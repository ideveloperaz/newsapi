//
//  CategoryTV.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit

// delegate that represents category table view behavioral pattern

class CategoryTV: NSObject, UITableViewDelegate {
    
    // this points to SourceDS data source to update on change
    
    weak var delegate: NACategoryChangedProtocol?
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCategory = NACategory.allValues[indexPath.row].rawValue
        
        // notify delegte on category changed event
    
        if let theDelegate = delegate {
            theDelegate.categoryDidChange(selectedCategory)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.isHidden = true
    }
    
}
