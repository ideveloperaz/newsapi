//
//  CategoryCell.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

// custom cell for category table view

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.gray
        self.textLabel?.textColor = UIColor.white
    }
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state 
    }
    
    class func reuseCellIdentifier() -> String {
        return "categoryCellIdentifier"
    }
    
}
