//
//  SourceCell.swift
//  NewsApi
//
//  Created by Rufat A on 2/23/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

// custom cell for source table view

class SourceCell: UITableViewCell {

    @IBOutlet weak var ivLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func reuseCellIdentifier() -> String {
        return "SourceCellIdentifier"
    }

}
